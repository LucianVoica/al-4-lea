package Pojos.Models;

import java.util.ArrayList;

import Pojos.Country;
import Pojos.Interfaces.SimpleObservable;

public class CountryModel extends SimpleObservable<CountryModel> {
    /**
     * The list of countries
     */
    private ArrayList<Country> countryList;

    /**
     * Get the list of countries
     * @return the list of the countries
     */
    public ArrayList<Country> getCountryList() {
        return countryList;
    }

    /**
     * Set a new list of countries
     * @param countryList the new list of countries
     */
    public void setCountryList(ArrayList<Country> countryList,boolean ...isNotificationRequired) {
        this.countryList = countryList;
        if(isNotificationRequired.length>0&&isNotificationRequired[0]==true)
            notifyObservers();
    }
}
