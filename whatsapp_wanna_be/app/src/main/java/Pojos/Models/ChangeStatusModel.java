package Pojos.Models;

import Pojos.Interfaces.SimpleObservable;

/**
 * Created by lucian on 14/7/2015.
 */
public class ChangeStatusModel extends SimpleObservable<ChangeStatusModel> {
    /**
     * The user's status
     */
    private String status;

    /**
     * Get the user's status
     * @return The user's status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the user's status
     * @param status The new user's status
     */
    public void setStatus(String status, boolean... isNotificationRequired) {
        this.status = status;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

}
