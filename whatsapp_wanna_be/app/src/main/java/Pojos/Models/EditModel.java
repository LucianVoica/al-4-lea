package Pojos.Models;

import android.net.Uri;
import android.provider.ContactsContract;

import java.util.Date;

import Pojos.Country;
import Pojos.Interfaces.SimpleObservable;

/**
 * Created by lucian on 14/7/2015.
 */
public class EditModel extends SimpleObservable<EditModel> {
    /**
     * The user's last name
     */
    private String lastName;
    /**
     *The user's first name
     */
    private String firstName;

    /**
     * The user's profile image
     */
    private Uri imageUri;

    /**
     * The user's birthday
     */
    private Date birthdayDate=new Date();

    /**
     * The user's gender (Male/Female)
     */
    private String gender;
    /**
     * The user's country
     */
    //private String country;
    /**
     * The user's phoneNumber;
     */
    private String phoneNumber;
    /**
     * The user's emailAddress
     */
    private String emailAddress;

    private Country country;
    /**
     * Get the user's last name
     * @return The last name
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * Set the user's last name
     * @param lastName The new user's last name
     */
    public void setLastName(String lastName, boolean... isNotificationRequired) {
        this.lastName = lastName;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

    /**
     * Get the user's first name
     * @return User's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the user's first name
     * @param firstName The new user's first name
     */
    public void setFirstName(String firstName, boolean... isNotificationRequired ) {
        this.firstName = firstName;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

    /**
     * Get user's profile image
     * @return The user's profile image
     */
    public Uri getImageUri() {
        return imageUri;
    }

    /**
     * Set the user's profile image
     * @param imageUri The user's new profile image
     * @param isNotificationRequired The notifier
     */
    public void setImageUri(Uri imageUri,boolean... isNotificationRequired) {
        this.imageUri = imageUri;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

    /**
     * Get the user's birthday
     * @return The user's birthday
     */
    public Date getBirthdayDate() {
        return birthdayDate;
    }

    /**
     * Set the user's birthday
     * @param birthdayDate The user's new birthday
     * @param isNotificationRequired The notifier
     */
    public void setBirthdayDate(Date birthdayDate,boolean... isNotificationRequired) {
        this.birthdayDate = birthdayDate;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender,boolean... isNotificationRequired) {
        this.gender = gender;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country, boolean... isNotificationRequired) {
        this.country = country;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber,boolean... isNotificationRequired) {
        this.phoneNumber = phoneNumber;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress,boolean... isNotificationRequired) {
        this.emailAddress = emailAddress;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

}
