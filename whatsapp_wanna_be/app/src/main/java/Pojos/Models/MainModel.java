package Pojos.Models;

import android.net.Uri;

import java.util.Date;

import Pojos.Interfaces.SimpleObservable;

/**
 * Created by lucian on 12/7/2015.
 */
public class MainModel extends SimpleObservable<MainModel>{
    /**
     * The user's last name
     */
    private String lastName;
    /**
     *The user's first name
     */
    private String firstName;

    /**
     * The user's status
     */
    private String status;

    /**
     * The user's profile image
     */
    private Uri profileImage;

    /**
     * The standard text "Nume utilizator"
     */
    private String standardUserName;

    /**
     * The user's birthday
     */
    private String birthday;
    /**
     * The user's gender
     */
    private String gender;
    /**
     * The user's country
     */
    private String country;
    /**
     * The user's phone number
     */
    private String phoneNumber;
    /**
     * The user's email address
     */
    private String emailAddress;
    /**
     * The constructor to initialize the default values
     */
    public MainModel() {
        lastName="Ex: Popescu";
        firstName="Ion";
        status ="STATUS\nAdd new status";
        standardUserName="Nume utilizator";
        birthday="Ex: 01/01/1990";
        gender="Ex: male";
        country="Ex: Romania";
        phoneNumber="076381...";
        emailAddress="Ex: popescu.ion@gmail.com";

    }
    /**
     * Get the user's last name
     * @return The last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the user's last name
     * @param lastName The new user's last name
     */
    public void setLastName(String lastName, boolean... isNotificationRequired) {
        this.lastName = lastName;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]){
            notifyObservers();
        }
    }

    /**
     * Get the user's status
     * @return The user's status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the user's status
     * @param status The new user's status
     */
    public void setStatus(String status, boolean... isNotificationRequired) {
        this.status = status;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    /**
     * Get the user's first name
     * @return User's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the user's first name
     * @param firstName The new user's first name
     */
    public void setFirstName(String firstName,boolean... isNotificationRequired  ) {
        this.firstName = firstName;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    /**
     * Get the user's profile picture
     * @return
     */
    public Uri getProfileImage() {
        return profileImage;
    }

    /**
     * Set the user's profile picture
     * @param profileImage The new user's profile picture
     * @param isNotificationRequired The notifier
     */
    public void setProfileImage(Uri profileImage,boolean... isNotificationRequired ) {
        this.profileImage = profileImage;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    /**
     * Get the sandard text "Nume utilzator"
     * @return
     */
    public String getStandardUserName() {
        return standardUserName;
    }

    /**
     * Set the standard text for "Nume utilizator"
      * @param isNotificationRequired The notifier
     */
    public void setStandardUserName(boolean... isNotificationRequired) {
        this.standardUserName = "Nume utilizator";
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    /**
     * Get the user's birthdat
     * @return
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * Set the user's birthday
     * @param birthday The new user's birthday
     * @param isNotificationRequired The notifier
     */
    public void setBirthday(String birthday,boolean... isNotificationRequired) {
        this.birthday = birthday;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender,boolean... isNotificationRequired) {
        this.gender = gender;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country,boolean... isNotificationRequired) {
        this.country = country;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber,boolean... isNotificationRequired) {
        this.phoneNumber = phoneNumber;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress,boolean... isNotificationRequired) {
        this.emailAddress = emailAddress;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }
}
