package Views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mycompany.whatsapp_wanna_be.R;

import Pojos.Country;
import Pojos.Interfaces.OnChangeListener;
import Pojos.Models.EditModel;
import Utils.Constants;


/**
 * Created by lucian on 14/7/2015.
 */
public class EditLayout extends RelativeLayout implements OnChangeListener<EditModel> {
    /**
     * The model
     */
    private EditModel model;

    /**
     * The edit button from the top of the screen
     */
    private Button editButton;

    /**
     * The view listener
     */
    private ViewListener viewListener;

    /**
     * The last name editText
     */
    EditText lastNameEditText;
    /**
     * The first name editText
     */
    EditText firstNameEditText;
    /**
     * The profile image
     */
    ImageView profileImageImageView;
    /**
     * The birthday
     */
    private TextView birthdayTextView;
    /**
     * The gender
     */
    private TextView genderTextView;
    /**
     * The user's country
     */
    private TextView countryTextView;
    /**
     * The user's phone number
     */
    private EditText phoneNumberEditText;
    /**
     * The user's e-mail address
     */
    private EditText emailAddressEditText;

    /**
     * The MainLayout constructor
     * @param context The context
     * @param attrs The attrs
     */
    public EditLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }

    /**
     * Initialize the views
     */
    public void initLayout() {
        lastNameEditText= (EditText) findViewById(R.id.edit_activity_new_last_name_edittext);
        firstNameEditText= (EditText) findViewById(R.id.edit_activity_new_first_name_edittext);
        this.birthdayTextView= (TextView) findViewById(R.id.activity_edit_profile_birthday_picker_textview);
        this.genderTextView= (TextView) findViewById(R.id.edit_activity_gender_textview);
        profileImageImageView=(ImageView)findViewById(R.id.edit_activity_profile_image_imageview);
        editButton= (Button) findViewById(R.id.edit_activity_edit_button_button);
        countryTextView=(TextView) findViewById(R.id.activity_edit_profile_country_picker_textview);
        phoneNumberEditText=(EditText) findViewById(R.id.edit_activity_phone_number_edittext);
        emailAddressEditText= (EditText) findViewById(R.id.edit_activity_email_address_edittext);

        birthdayTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().OnBirthdayTextViewClicked();
            }
        });

        genderTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().OnGenderTextViewClicked();
            }
        });

        profileImageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getViewListener().OnProfileImageClicked();

            }
        });

        editButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().OnEditActivityEditButtonClicked();
            }
        });

        countryTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onCountryTextViewClicked();
            }
        });

    }

    /**
     * Get the user's new first name
     * @return User's new first name
     */
    public String getFirstName() {
        return firstNameEditText.getText().toString();
    }

    /**
     * Get the user's new last name
     * @return User's new last name
     */
    public String getLastName() {
        return lastNameEditText.getText().toString();
    }

    /**
     * The user's phone number
     * @return The user's phone number
     */
    public String getPhoneNumberEditText() {
        return phoneNumberEditText.getText().toString();
    }

    /**
     * The user's email address
     * @return The user's email address
     */
    public String getEmailAddressEditText() {
        return emailAddressEditText.getText().toString();
    }

    public String getCountry() {
        return countryTextView.getText().toString();
    }
    /**
     * Update the layout
     */
    public void updateLayout() {
        lastNameEditText.setText(getModel().getLastName());
        firstNameEditText.setText(getModel().getFirstName());
        profileImageImageView.setImageURI(getModel().getImageUri());
        birthdayTextView.setText(Constants.simpleDateFormat_ddMMyyyy.format(this.getModel().getBirthdayDate()));
        if(getModel().getGender()!=" ") {
            genderTextView.setText(getModel().getGender());
        }
        else {
            genderTextView.setText("Male");
        }
       // countryEditText.setText(getModel().getCountry());
        phoneNumberEditText.setText(getModel().getPhoneNumber());
        emailAddressEditText.setText(getModel().getEmailAddress());
        if(getModel().getCountry()== null){
            getModel().setCountry(new Country());
        }
        Log.i("EditLayout", "" + getModel().getCountry().getName());
        countryTextView.setText(getModel().getCountry().getName());
    }

    /**
     * Get the model
     * @return The model
     */
    public EditModel getModel() {
        return model;
    }

    /**
     * Set the model
     * @param model New model value
     */
    public void setModel(EditModel model) {
        this.model = model;
        this.model.addListener(this);
        updateLayout();
    }

    /**
     * Get the view listener
     * @return the view listener
     */
    public ViewListener getViewListener() {
        return viewListener;
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }


    /**
     * Update the layout
     */
    @Override
    public void onChange() {
        updateLayout();
    }

    /**
     * Get the birthday date
     * @return The user's birthday
     */
    public String getBirthdayTextView (){return this.birthdayTextView.getText().toString();}

    /**
     *Get the user's gender
     * @return the content of the gender text view as a String
     */
    public String getGenderTextView(){
        return this.genderTextView.getText().toString();
    }

    public interface ViewListener {
        public void OnProfileImageClicked();
        public void OnEditActivityEditButtonClicked();
        public void saveProfile();
        public void sendProfile();
        public void OnBirthdayTextViewClicked();
        public void saveData();
        public void OnGenderTextViewClicked();
        void onCountryTextViewClicked();
    }
}
