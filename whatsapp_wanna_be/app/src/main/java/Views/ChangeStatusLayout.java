package Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.mycompany.whatsapp_wanna_be.R;

import Pojos.Interfaces.OnChangeListener;
import Pojos.Models.ChangeStatusModel;
import Pojos.Models.EditModel;

/**
 * Created by lucian on 15/7/2015.
 */
public class ChangeStatusLayout extends RelativeLayout implements OnChangeListener<ChangeStatusModel> {
    /**
     * The model
     */
    private ChangeStatusModel model;

    /**
     * The button from the top of the screen
     */
    private Button finishButton;

    /**
     * The view listener
     */
    private ViewListener viewListener;

    /**
     * The status
     */
    EditText statusEditText;

    /**
     * The constructor for ChangeStatusLayout
     * @param context The context
     * @param attrs The attributeSet
     */
    public ChangeStatusLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }

    /**
     * Initialize the views
     */
    public void initLayout() {

        statusEditText=(EditText) findViewById(R.id.change_status_activity_new_status_edittext);

        finishButton= (Button) findViewById(R.id.change_status_finish_button_button);
        finishButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().OnChangeActivityFinishButtonClicked();
            }
        });
    }

    /**
     * Get the user's status
     * @return The user's status
     */
    public String getStatus() {
        return statusEditText.getText().toString();
    }

    /**
     * Update the user's status
     */
    public void updateLayout() {
        statusEditText.setText(getModel().getStatus());
    }

    /**
     * Get the model
     * @return The model
     */
    public ChangeStatusModel getModel() {
        return model;
    }

    /**
     * Set the model
     * @param model New model value
     */
    public void setModel(ChangeStatusModel model) {
        this.model = model;
        this.model.addListener(this);
        updateLayout();
    }

    /**
     * Get the view listener
     * @return the view listener
     */
    public ViewListener getViewListener() {
        return viewListener;
    }

    /**
     * Set the viewListener
     * @param viewListener The new viewListener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    /**
     * Update the layout
     */
    @Override
    public void onChange() {
        updateLayout();
    }

    public interface ViewListener {
        public void OnChangeActivityFinishButtonClicked();
        public void saveProfile();
        public void sendProfile();
    }
}
