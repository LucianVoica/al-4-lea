package Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mycompany.whatsapp_wanna_be.R;

import java.util.Date;

import Pojos.Interfaces.OnChangeListener;
import Pojos.Models.MainModel;
import Utils.Constants;

/**
 * Created by lucian on 12/7/2015.
 */
public class MainLayout extends RelativeLayout implements OnChangeListener<MainModel> {
    /**
     * The model
     */
    private MainModel model;

    public TextView getUserNameTextView() {
        return userNameTextView;
    }

    public void setUserNameTextView(String userNameTextView) {
        this.userNameTextView.setText(userNameTextView);
    }

    /**
     * The user's name
     */
    private TextView userNameTextView;

    /**
     * Standard message for user's name
     */
    private TextView standardUserName;

    /**
     * The user's status
     */
    private TextView statusTextView;

    /**
     * The user's profile image
     */
    private ImageView profileImageImageView;

    /**
     * The user's birthday
     */
    private TextView birthdayTextView;

    public void setGenderTextView(String genderTextView) {
        this.genderTextView .setText( genderTextView);
    }

    public void setBirthdayTextView(String birthdayTextView) {
        this.birthdayTextView.setText(birthdayTextView);
    }

    /**
     * The user's gender
     */
    private TextView genderTextView;

    /**
     * The user's country
     */
    private TextView countryTextView;

    /**
     * The user's gender
     */
    private TextView phoneNumberTextView;

    public void setEmailTextView(String emailTextView) {
        this.emailTextView.setText(emailTextView);
    }

    /**
     * The user's gender
     */
    private TextView emailTextView;

    /**
     * The image button from the top of the screen
     */
    private Button editButton;

    /**
     * The view listener
     */
    private ViewListener viewListener;

    /**
     * The MainLayout constructor
     * @param context The context
     * @param attrs The attrs
     */
    public MainLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }

    /**
     * Initialize the views
     */
    public void initLayout() {
        standardUserName= (TextView) findViewById(R.id.activity_main_standard_user_name_textview);
        editButton= (Button) findViewById(R.id.activity_main_edit_button);
        editButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().OnEditButtonClicked();
            }
        });
        userNameTextView = (TextView) findViewById(R.id.activity_main_last_name_textview);
        profileImageImageView= (ImageView) findViewById(R.id.activity_main_profile_profile_picture_imagebutton);
        statusTextView= (TextView) findViewById(R.id.activity_main_status_textview);
        statusTextView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                getViewListener().OnStatusTextViewClicked();
            }
        });
        birthdayTextView=(TextView) findViewById(R.id.activity_main_birthday_textview);
        genderTextView=(TextView) findViewById(R.id.activity_main_gender_textview);
        countryTextView= (TextView) findViewById(R.id.activity_main_country_textview);
        phoneNumberTextView=(TextView) findViewById(R.id.activity_main_phoneNumber_textview);
        emailTextView= (TextView) findViewById(R.id.activity_main_email_textview);
    }

    /**
     * Update the layout
     */
    public void updateLayout() {
        standardUserName.setText(getModel().getStandardUserName()+"");
        userNameTextView.setText(getModel().getLastName() + " " + getModel().getFirstName());
        statusTextView.setText(getModel().getStatus() + "");
        profileImageImageView.setImageURI(getModel().getProfileImage());
        birthdayTextView.setText(getModel().getBirthday()+"");
        genderTextView.setText(getModel().getGender()+"");
        countryTextView.setText(getModel().getCountry()+"");
        phoneNumberTextView.setText(getModel().getPhoneNumber()+"");
        emailTextView.setText(getModel().getEmailAddress()+"");
    }

    /**
     * Get the model
     * @return The model
     */
    public MainModel getModel() {
        return model;
    }

    /**
     * Set the model
     * @param model New model value
     */
    public void setModel(MainModel model) {
        this.model = model;
        this.model.addListener(this);
        updateLayout();
    }

    /**
     * Get the view listener
     * @return the view listener
     */
    public ViewListener getViewListener() {
        return viewListener;
    }

    /**
     * Set the view listener
     * @param viewListener The new view listener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    /**
     * Update the layout
     */
    @Override
    public void onChange() {
        updateLayout();
    }

    public interface ViewListener {
        public void OnEditButtonClicked();
        public void OnStatusTextViewClicked();
    }
}
