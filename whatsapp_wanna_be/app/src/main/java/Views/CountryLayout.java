package Views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mycompany.whatsapp_wanna_be.R;

import Pojos.Interfaces.OnChangeListener;
import Pojos.Models.CountryModel;
import Views.Adapters.CountryAdapter;

/**
 * Created by lucian on 24/7/2015.
 */
public class CountryLayout extends RelativeLayout implements OnChangeListener <CountryModel>{

    /**
     * The country model
     */
    private CountryModel countryModel;
    /**
     * The list view of countries
     */
    private ListView countriesListView;
    /**
     * The country name edit text
     */
    private EditText countryNameEditText;
    /**
     * The view listener
     */
    private ViewListener viewListener;
    /**
     * The country adapter
     */
    private CountryAdapter countryAdapter;
    /**
     * Country layout's constructor
     * @param context
     */
    public CountryLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Initialize the layout's views
     */
    public void initLayout() {
        countriesListView= (ListView) findViewById(R.id.activity_country_countries_listview);
        countryNameEditText= (EditText) findViewById(R.id.activity_country_country_name_edittext);

        countryNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                getViewListener().OnTextChanged();
            }
        });
        countriesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getViewListener().OnCountryItemClicked(position);

            }
        });

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initLayout();
    }

    /**
     * Get the country model
     * @return the country model
     */
    public CountryModel getCountryModel(){
        return this.countryModel;
    }

    /**
     * Set the country model
     * @param countryModel
     */
    public void setCountryModel(CountryModel countryModel){
        this.countryModel=countryModel;
        this.countryModel.addListener(this);
    }

    /**
     * Get the view listener
     * @return
     */
    public ViewListener getViewListener() {
        return viewListener;
    }
    /**
     * Set a new view listener
     * @param viewListener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    @Override
    public void onChange() {
        updateAdapter();
    }

    /**
     * Get the country name edit text's content as a String
     * @return the country name edit text's content as a String
     */
    public String getCountryNameEditText(){
        return countryNameEditText.getText().toString();
    }

    /**
     * Get the country adapter
     * @return the country adapter
     */
    public CountryAdapter getCountryAdapter() {
        return countryAdapter;
    }

    /**
     * Set a new country adapter
     * @param countryAdapter the new country adapter
     */
    public void setCountryAdapter(CountryAdapter countryAdapter) {
        this.countryAdapter = countryAdapter;
        this.countriesListView.setAdapter(countryAdapter);
    }

    /**
     * Update the adapter's list of countries with the current country model's list
     * Notify the attached observers that the data has been changed and any view reflecting the data
     * should refresh itself
     */
    private void updateAdapter(){
        if(countryAdapter != null) {
            countryAdapter.setNewCountries(this.countryModel.getCountryList());
            countryAdapter.notifyDataSetChanged();
        }
        else {
            countryAdapter=new CountryAdapter(getContext(),this.countryModel.getCountryList());
        }

    }
    public interface ViewListener {
        void OnTextChanged();
        void OnCountryItemClicked(int position);
    }
}
