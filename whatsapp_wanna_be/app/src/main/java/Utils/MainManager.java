package Utils;

import android.util.Log;

import Views.MainLayout;

/**
 * Created by lucian on 24/7/2015.
 */
public class MainManager {
    private static volatile MainManager instance;
    private CountryManager countryManager;


    private MainManager(){

        countryManager=CountryManager.getInstance();
    }
    public static MainManager getInstance(){
        if(instance==null)
        {
            synchronized (CountryManager.class)
            {
                if(instance==null)
                    instance=new MainManager();

            }
        }
        return instance;
    }
    public CountryManager getCountryManager(){
        Log.i("MainManager","getCountryManager");
        return countryManager;

    }
}

