package Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Pojos.Models.CountryModel;

/**
 * Created by lucian on 13/7/2015.
 */
public class Constants {
    public static String lastName;
    public static final String EXTRA_FIRST_NAME="editFirstName";
    public static final String EXTRA_LAST_NAME="editLastName";
    public static final String EXTRA_STATUS="editStatus";
    public static SimpleDateFormat simpleDateFormat_MMMddyyyy=new SimpleDateFormat("MMM dd,yyyy");
    public static SimpleDateFormat simpleDateFormat_ddMMyyyy=new SimpleDateFormat("dd/MM/yyyy");
    public static List<String> genders= Arrays.asList("Female", "Male");
    //public static ArrayList<CountryModel> countryArrayList = new ArrayList<>();
}

