package com.mycompany.whatsapp_wanna_be;

import android.content.Intent;

import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;

import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import Pojos.Models.MainModel;
import Utils.Constants;
import Views.MainLayout;


public class MainActivity extends ActionBarActivity {

    private MainModel rootModel;
    private MainLayout rootLayout;
    private TextView info;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private Profile profile;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private String email;
    private String gender;
    private String birthday;
    private String locale;

    private MainLayout.ViewListener viewListener=new MainLayout.ViewListener() {

        /**
         * Starts the edit activity
         */
        @Override
        public void OnEditButtonClicked() {
            Intent intent = new Intent(MainActivity.this, EditActivity.class);
            startActivityForResult(intent, 1);
        }


        /**
         * Starts the changing status activity
         */
        @Override
        public void OnStatusTextViewClicked() {
            Intent intent = new Intent(MainActivity.this, ChangeStatusActivity.class);
            startActivityForResult(intent, 2);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        rootModel = new MainModel();
        rootLayout= (MainLayout) View.inflate(MainActivity.this, R.layout.activity_main, null);
        setContentView(rootLayout);

        info = (TextView)findViewById(R.id.info);
        loginButton = (LoginButton)findViewById(R.id.login_button);

        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends, user_location"));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                profile = Profile.getCurrentProfile();
                if (profile != null) {
                    rootLayout.setUserNameTextView(profile.getName());
                    info.setText("Welcome " + profile.getName());
                }
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                Log.v("LoginActivity", response.toString());
                                email =  object.optString("email");
                                gender =  object.optString("gender");
                                birthday = object.optString("birthday");
                                locale = object.optString("locale");
                                rootLayout.setGenderTextView(gender);
                                rootLayout.setBirthdayTextView(birthday);
                                rootLayout.setEmailTextView(email);
                                info.append(" loc "+locale);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,locale");
                request.setParameters(parameters);
                request.executeAsync();

            }


            @Override
            public void onCancel() {
                info.setText("Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException e) {
                info.setText("Login attempt failed.");
            }
        });



        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                if(newProfile != null) {
                    rootLayout.setUserNameTextView(profile.getName());
                    info.setText("Welcome " + profile.getName());
                    rootLayout.setGenderTextView(gender);
                    rootLayout.setBirthdayTextView(birthday);
                    rootLayout.setEmailTextView(email);
                    info.append(" loc " + locale);
                }
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
        rootLayout.setModel(rootModel);
        rootLayout.setViewListener(viewListener);



    }

    /**
     * Sets the edited informations
     * @param requestCode Request code
     * @param resultCode Result code
     * @param data Intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1)
            if(resultCode==RESULT_OK) {
                String lastName=data.getStringExtra(Constants.EXTRA_LAST_NAME);
                String firstName=data.getStringExtra(Constants.EXTRA_FIRST_NAME);
                Uri profileImage= (Uri) data.getExtras().get("poza");
                Date birthday= (Date) data.getExtras().get("data");
                String gender=data.getStringExtra("sex");
                String phoneNumber=data.getStringExtra("phone");
                String country=data.getStringExtra("country");
                String email=data.getStringExtra("email");

                rootModel.setLastName(lastName,true);
                rootModel.setFirstName(firstName, true);
                rootModel.setProfileImage(profileImage, true);
                rootModel.setBirthday(Constants.simpleDateFormat_ddMMyyyy.format(birthday) ,true);
                rootModel.setGender(gender,true);
                rootModel.setCountry(country,true);
                rootModel.setPhoneNumber(phoneNumber,true);
                rootModel.setEmailAddress(email,true);
            }
        if(requestCode==2)
            if(resultCode==RESULT_OK) {
                String status=data.getStringExtra(Constants.EXTRA_STATUS);
                rootModel.setStatus("STATUS\n"+status, true);
            }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();

    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        if(profile != null) {
            rootLayout.setUserNameTextView(profile.getName());
            info.setText("Welcome"+profile.getName());
            rootLayout.setGenderTextView(gender);
            rootLayout.setBirthdayTextView(birthday);
            rootLayout.setEmailTextView(email);
            info.append(" loc "+locale);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
