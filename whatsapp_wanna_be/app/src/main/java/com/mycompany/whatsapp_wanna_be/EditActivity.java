package com.mycompany.whatsapp_wanna_be;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import Pojos.Country;
import Pojos.Models.CountryModel;
import Pojos.Models.EditModel;
import Pojos.Models.MainModel;
import Utils.Constants;
import Views.EditLayout;


/**
 * Created by lucian on 13/7/2015.
 */
public class EditActivity extends ActionBarActivity {
    /**
     * The model
     */
    private EditModel rootModel;
    /**
     * The layout
     */
    private EditLayout rootLayout;

    private MainModel mainModel;

    EditLayout.ViewListener viewListener=new EditLayout.ViewListener() {

        /**
         * Edit the profile image
         */
        @Override
        public void OnProfileImageClicked() {
            Intent intent=new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            saveProfile();
            startActivityForResult(Intent.createChooser(intent, "Select Profile Image"),1);

        }

        /**
         * Saves and sends user's informations
         */
        @Override
        public void OnEditActivityEditButtonClicked() {
            saveProfile();
            sendProfile();
       }

        /**
         * Saves the user's informations
         */
        @Override
        public void saveProfile() {
            rootModel.setFirstName(rootLayout.getFirstName());
            rootModel.setLastName(rootLayout.getLastName());
            try {
                rootModel.setBirthdayDate(Constants.simpleDateFormat_ddMMyyyy.parse(rootLayout.getBirthdayTextView()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            rootModel.getCountry().setName(rootLayout.getCountry());
            rootModel.setPhoneNumber(rootLayout.getPhoneNumberEditText());
            rootModel.setEmailAddress(rootLayout.getEmailAddressEditText());
        }

        /**
         * Sends the user's informations
         */
        @Override
        public void sendProfile() {
            Intent intent=new Intent();
            intent.putExtra(Constants.EXTRA_LAST_NAME,rootModel.getLastName());
            intent.putExtra(Constants.EXTRA_FIRST_NAME,rootModel.getFirstName());
            intent.putExtra("poza",rootModel.getImageUri());
            intent.putExtra("data",rootModel.getBirthdayDate());
            intent.putExtra("sex",rootModel.getGender());
            intent.putExtra("country", rootModel.getCountry().getName());
            intent.putExtra("phone", rootModel.getPhoneNumber());
            intent.putExtra("email",rootModel.getEmailAddress());
            setResult(RESULT_OK, intent);
            EditActivity.this.finish();
        }

        @Override
        public void OnBirthdayTextViewClicked() {
            saveProfile();
            Calendar calendar=Calendar.getInstance();
            calendar.setTime(rootModel.getBirthdayDate());
            DatePickerDialog datePickerDialog= new DatePickerDialog(EditActivity.this,dateSetListener,calendar.get(Calendar.DAY_OF_MONTH),calendar.get(Calendar.MONTH),calendar.get(calendar.YEAR));
            datePickerDialog.show();
        }

        @Override
        public void saveData() {

            rootModel.setFirstName(rootLayout.getFirstName());
            rootModel.setLastName(rootLayout.getLastName());
            try {
                rootModel.setBirthdayDate(Constants.simpleDateFormat_ddMMyyyy.parse(rootLayout.getBirthdayTextView()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            rootModel.setGender(rootLayout.getGenderTextView());
            rootModel.getCountry().setName(rootLayout.getCountry());
            rootModel.setPhoneNumber(rootLayout.getPhoneNumberEditText());
            rootModel.setEmailAddress(rootLayout.getEmailAddressEditText());
        }

        @Override
        public void OnGenderTextViewClicked() {
            saveProfile();
            AlertDialog.Builder builder = new AlertDialog.Builder(EditActivity.this);
            builder.setItems((String[]) Constants.genders.toArray(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    rootModel.setGender(Constants.genders.get(item), true);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        @Override
        public void onCountryTextViewClicked() {
            saveData();
            Intent i=new Intent(EditActivity.this,CountryActivity.class);
            startActivityForResult(i, 2);
        }


    };

    private DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar=Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);
            Date date=calendar.getTime();
            rootModel.setBirthdayDate(date, true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        initModel();
        rootLayout= (EditLayout) View.inflate(EditActivity.this, R.layout.edit_activity, null);
        setContentView(rootLayout);
        rootLayout.setModel(rootModel);
        rootLayout.setViewListener(viewListener);

    }

    /**
     * Initialize the model
     */
    private void initModel() {
        rootModel = new EditModel();
        rootModel.setCountry(new Country());
    }

    /**
     * Set the profile image
     * @param reqCode Request code
     * @param resCode Restult code
     * @param data The intent
     */
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if(resCode == RESULT_OK) {
            if(reqCode == 1) {
                rootModel.setImageUri(data.getData(),true);
            }
            if(reqCode == 2) {
                rootModel.setCountry(new Country(data.getExtras().getString("countryName"),data.getExtras().getString("countryDialCode"),data.getExtras().getString("countryCode")),true);
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
