package com.mycompany.whatsapp_wanna_be;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import Pojos.Models.ChangeStatusModel;
import Utils.Constants;
import Views.ChangeStatusLayout;

import static com.mycompany.whatsapp_wanna_be.R.id.activity_main_status_textview;

/**
 * Created by lucian on 14/7/2015.
 */
public class ChangeStatusActivity extends ActionBarActivity {
    /**
     * The model
     */
    private ChangeStatusModel rootModel;
    /**
     * The layout
     */
    private ChangeStatusLayout rootLayout;

    ChangeStatusLayout.ViewListener viewListener= new ChangeStatusLayout.ViewListener() {

        /**
         * Save and send profile informations
         */
        @Override
        public void OnChangeActivityFinishButtonClicked() {
            saveProfile();
            sendProfile();
        }

        /**
         * Save profile's informations
         */
        @Override
        public void saveProfile() {
            rootModel.setStatus(rootLayout.getStatus());
        }

        /**
         * Send profile informations to main activity
         */
        @Override
        public void sendProfile() {
            Intent intent=new Intent();
            intent.putExtra(Constants.EXTRA_STATUS,rootModel.getStatus());
            setResult(RESULT_OK, intent);
            ChangeStatusActivity.this.finish();
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        rootModel = new ChangeStatusModel();
        rootLayout= (ChangeStatusLayout) View.inflate(ChangeStatusActivity.this, R.layout.change_status_activity, null);
        setContentView(rootLayout);
        rootLayout.setModel(rootModel);
        rootLayout.setViewListener(viewListener);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
