package com.mycompany.student;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.ListViewCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.google.gson.Gson;

import org.apache.http.client.ClientProtocolException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import POJO.Student;
import Utils.Constants;
import Views.Adapters.StudentsAdapter;


public class MainActivity extends ActionBarActivity {

    private final String TAG="MainActivity";
    private ListView studentsListView;
    private StudentsAdapter studentsAdapter;
    private String json="[{\"firstName\":\"first name1\",\"lastName\":\"last name1\",\"note\":10}," +
            "{\"firstName\":\"first name2\",\"lastName\":\"last name2\",\"note\":4}]";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "s-a creeat");

        initLayout();
        sendGet();
        gson();
        addStudentsFromGson();
    }

    private ArrayList<Student> getStudents() {
        Student s=new Student();
        s.setFirstName("voica");
        s.setLastName("lucian");
        s.setNote(10);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);
        Constants.studentArrayList.add(s);

        Student s1=new Student();
        s1.setFirstName("pieptea");
        s1.setLastName("lavinia");
        s1.setNote(9);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);
        Constants.studentArrayList.add(s1);

        return Constants.studentArrayList;
    }

    private void initLayout() {
        studentsListView=(ListView) findViewById(R.id.activity_main_students_listview);
        studentsAdapter=new StudentsAdapter(MainActivity.this,getStudents());
        studentsListView.setAdapter(studentsAdapter);

        Button button = (Button) findViewById(R.id.activity_main_add_button_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,SecondActivity.class);
                startActivity(intent);
            }
        });

        Button refresh=(Button) findViewById(R.id.activity_main_refresh_button);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(Student student:addStudentsFromGson()) {
                    Constants.studentArrayList.add(student);
                }
                studentsAdapter.setNewStudent(Constants.studentArrayList);
                studentsAdapter.notifyDataSetChanged();
            }
        });

    }

    private void gson()
    {
        final Gson gson = new Gson();
// original object instantiation
        Student modelObject = new Student();
        modelObject.setFirstName("first name");
        modelObject.setLastName("last name");
        modelObject.setNote(10);
        Log.i("MainActivity", "toJson ---");
        Log.i("MainActivity", "Original Java object : " + modelObject);
// converting an object to json object
        String json = gson.toJson(modelObject);
        Log.i("MainActivity", "Converted JSON string is : " + json);

        Log.i("MainActivity", "fromJson----");
// getting object from json representation
        Log.i("MainActivity", "Original JSON string is : " + json);
// converting json to object
        Student modelObject1 = gson.fromJson(json, Student.class);
        Log.i("MainActivity", "Converted Java object : " + modelObject1);
    }

    private Student[] addStudentsFromGson() {
        Gson gson=new Gson();
        Student[] s2=gson.fromJson(json,Student[].class);
        for(int i=0;i<2;i++)
            Log.i(TAG,""+s2[i]);
        return s2;
    }

    private void sendGet()
    {
        Log.i("MainActivity", "sending get");
        String URL = "http://indyvision-storage.appspot.com/phrase_classic?action=get&target=excuse_generator&filter=abc&format=json";
        RequestTask requestTask = new RequestTask();
        requestTask.execute(URL);
        Log.i("MainActivity", "executed");
    }

    /**
     * Get a request
     */
    class RequestTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... uri) {
            String responseString = null;
            try {

                URL url = new URL(uri[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                if(conn.getResponseCode() == HttpsURLConnection.HTTP_OK){
                    // Do normal input or output stream reading
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                    Log.i("MainActivity", sb.toString());

                }
                else {
                    Log.i("MainActivity", "failed");
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //Do anything with response..
        }
    }

    private void refresh()
    {
        if(studentsAdapter != null)
        {
            studentsAdapter.setNewStudent(Constants.studentArrayList);
            studentsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        refresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
