package com.mycompany.student;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import POJO.Student;
import Utils.Constants;

/**
 * Created by lucian on 6/7/2015.
 */

public class SecondActivity extends ActionBarActivity {

    EditText firstNameEditText;
    EditText lastNameEditText;
    EditText noteEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        Button button ;
        button= (Button) findViewById(R.id.second_activity_add_button_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }
    public void onButtonClick(View v) { save(); }

    private void save()
    {   firstNameEditText= (EditText) findViewById(R.id.second_activity_new_first_name_edittext);
        lastNameEditText= (EditText) findViewById(R.id.second_activity_new_last_name_edittext);
        noteEditText=(EditText) findViewById(R.id.second_activity_new_note_edittext);

        String firstName = firstNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();
        int note=Integer.parseInt(noteEditText.getText().toString());

        Student s = new Student();
        s.setFirstName(firstName);
        s.setLastName(lastName);
        s.setNote(note);

        Constants.studentArrayList.add(s);
        SecondActivity.this.finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}