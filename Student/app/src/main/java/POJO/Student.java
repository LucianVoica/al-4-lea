package POJO;

import android.app.Activity;
import android.util.Log;

import com.mycompany.student.MainActivity;

/**
 * Created by lucian on 6/7/2015.
 */
public class Student {
    /**
     * Student's first name
     */
    private String firstName;
    /**
     * Student's last name
     */
    private String lastName;
    /**
     * Student's note
     */
    private int note;

    /**
     * Gets the student's first name
     * @return the student's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the student's first name
     * @param firstName Student's first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the student's last name
     * @return the student's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the student's last name
     * @param lastName Student's last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the student's note
     * @return the student's note
     */
    public int getNote() {
        return note;
    }

    /**
     * Sets student's note
     * @param note Student's note
     * @return true if the note was set, false otherwise
     */
    public boolean setNote(int note) {
        if(note > 0 && note <=10) {
            this.note = note;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return lastName+" "+firstName+" "+note+"\n";
    }

}
