package Views.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mycompany.student.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

import POJO.Student;

/**
 * Created by lucian on 6/7/2015.
 */
public class StudentsAdapter extends BaseAdapter {
    /**
     * The context
     */
    private Context context;
    /**
     * The student List
     */
    private ArrayList<Student> items;
    /**
     * The layout inflater
     */
    private LayoutInflater layoutInflater;

    /**
     * The constructor for the students adapter
     * @param context the context
     * @param items the students list
     */
    public StudentsAdapter(Context context,ArrayList<Student> items) {
        this.layoutInflater=((Activity)context).getLayoutInflater();
        this.context=context;
        this.items=items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null) {

            convertView=layoutInflater.inflate(R.layout.student_layout,parent,false);
            holder=new ViewHolder();

            holder.firstNameTextview=(TextView) convertView.findViewById(R.id.student_layout_first_name_textview);
            holder.lastNameTextview=(TextView) convertView.findViewById(R.id.student_layout_last_name_textview);
            holder.noteTextview=(TextView) convertView.findViewById(R.id.student_layout_note_textview);

            convertView.setTag(holder);
        } else {
            holder=(ViewHolder) convertView.getTag();
        }

        holder.firstNameTextview.setText(items.get(position).getFirstName());
        holder.lastNameTextview.setText(items.get(position).getLastName());
        holder.noteTextview.setText(items.get(position).getNote()+"");
        if(items.get(position).getNote()<5) {
            holder.noteTextview.setTextColor(context.getResources().getColor(R.color.red));
        } else {
            holder.noteTextview.setTextColor(context.getResources().getColor(R.color.black));
        }



        return convertView;
    }

    public void setNewStudent(ArrayList<Student> newStudent) {
        this.items = newStudent;
    }

    public class ViewHolder {
        TextView firstNameTextview;
        TextView lastNameTextview;
        TextView noteTextview;
    }

}
