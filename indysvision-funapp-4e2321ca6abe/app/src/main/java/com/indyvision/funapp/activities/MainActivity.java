package com.indyvision.funapp.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.indyvision.funapp.R;
import com.indyvision.funapp.pojo.models.MainActivityModel;
import com.indyvision.funapp.views.MainActivityLayout;


public class MainActivity extends ActionBarActivity {

    /**
     * The model
     */
    private MainActivityModel rootModel;

    /**
     * The layout
     */
    private MainActivityLayout rootLayout;

    /**
     * The view listener
     */
    private MainActivityLayout.ViewListener viewListener=new MainActivityLayout.ViewListener() {

        @Override
        public void onPlayer1ButtonClicked() {
            if (rootModel.getContorPlayer1() == -1) {
                rootModel.setContorPlayer1(rootModel.getContorPlayer1() + 1, true);
                rootModel.setContorPlayer2(0,true);
            }
            else {
                rootModel.setContorPlayer1(rootModel.getContorPlayer1()+1,true);
            }
        }
        @Override
        public void onPlayer2ButtonClicked() {
            if (rootModel.getContorPlayer2() == -1) {
                rootModel.setContorPlayer2(rootModel.getContorPlayer2() + 1,true);
                rootModel.setContorPlayer1(0,true);
            }
            rootModel.setContorPlayer2(rootModel.getContorPlayer2() + 1,true);
        }

        @Override
        public void onReplayPlayerButtonClicked() {
            rootModel.setContorPlayer1(-1, true);
            rootModel.setContorPlayer2(-1, true);
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rootModel = new MainActivityModel();
        rootLayout= (MainActivityLayout) View.inflate(MainActivity.this, R.layout.activity_main, null);
        setContentView(rootLayout);
        rootLayout.setModel(rootModel);
        rootLayout.setViewListener(viewListener);
    }
}
