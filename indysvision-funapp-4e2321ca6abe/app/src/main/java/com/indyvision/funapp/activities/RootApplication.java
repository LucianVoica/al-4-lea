package com.indyvision.funapp.activities;

import android.app.Application;

import com.indyvision.funapp.managers.MainManager;
import com.indyvision.funapp.utils.FontUtils;

/**
 * Created by cristian.baita on 7/17/2015.
 */
public class RootApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        FontUtils.init(this);
        MainManager.getInstance();
    }
}
