package com.indyvision.funapp.views.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.indyvision.funapp.utils.FontUtils;

/**
 * Created by cristian.baita on 7/17/2015.
 */
public class FunButton extends Button{

    public FunButton(Context context) {
        super(context);
        init();
    }

    public FunButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FunButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        setTypeface(FontUtils.getRegularCactusTf());
    }
}
