package com.indyvision.funapp.utils;

import android.content.Context;
import android.graphics.Typeface;

public class FontUtils {

	private static Typeface regularCactusTf;
    private static Typeface regularStrawTf;
    private static Typeface regularCrystalTf;

	public static void init(Context context)
	{
		regularCactusTf = Typeface.createFromAsset(context.getAssets(), "fonts/Cactus Love.ttf");
		regularStrawTf = Typeface.createFromAsset(context.getAssets(), "fonts/Chewed Straw.ttf");
		regularCrystalTf = Typeface.createFromAsset(context.getAssets(), "fonts/crystal-webfont.ttf");
	}

    public static Typeface getRegularCactusTf() {
        return regularCactusTf;
    }

    public static Typeface getRegularStrawTf() {
        return regularStrawTf;
    }

    public static Typeface getRegularCrystalTf() {
        return regularCrystalTf;
    }
}
