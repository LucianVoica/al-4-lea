package com.indyvision.funapp.managers;

import com.indyvision.funapp.utils.FontUtils;

/**
 * Created by cristian.baita on 7/17/2015.
 */
public class MainManager {
    private static MainManager instance = null;

    private CacheManager cacheManager;
    private DbManager dbManager;
    private DownloadManager downloadManager;
    private SyncManager syncManager;

    private MainManager(){
        cacheManager = new CacheManager();
        dbManager = new DbManager();
        downloadManager = new DownloadManager();
        syncManager = new SyncManager();

        // init anything that needs it
    }

    public static MainManager getInstance(){
        if (instance == null)
        {
            instance = new MainManager();
        }

        return instance;
    }

}
