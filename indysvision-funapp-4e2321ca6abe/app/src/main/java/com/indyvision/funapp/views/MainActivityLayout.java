package com.indyvision.funapp.views;

import android.content.Context;
import android.opengl.Visibility;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.indyvision.funapp.R;
import com.indyvision.funapp.pojo.Interfaces.OnChangeListener;
import com.indyvision.funapp.pojo.models.MainActivityModel;
import com.indyvision.funapp.views.custom.FunButton;
import com.indyvision.funapp.views.custom.FunTextView;

/**
 * Created by cristian.baita on 7/17/2015.
 */
public class MainActivityLayout extends RelativeLayout implements OnChangeListener<MainActivityModel> {
    /**
     * The context
     */
    private Context context;
    /**
     * The model
     */
    private MainActivityModel model;

    /**
     * Player1 's message
     */
    private TextView player1TextView;

    /**
     * Player2 's message
     */
    private TextView player2TextView;

    /**
     * The player1's button
     */
    private FunButton button1Button;

    /**
     * The player2's button
     */
    private  FunButton button2Button;

    /**
     * The player1's replay button
     */
    private FunButton replayPlayer1Button;

    /**
     * The player2's replay button
     */
    private FunButton replayPlayer2Button;

    /**
     * The view listener
     */
    private ViewListener viewListener;

    /**
     * Get the model
     * @return The model
     */
    public MainActivityModel getModel() {
        return model;
    }
    /**
     * Set the model
     * @param model New model value
     */
    public void setModel(MainActivityModel model) {
        this.model = model;
        this.model.addListener(this);
        updateLayout();
    }

    /**
     * Get the view listener
     * @return the view listener
     */
    public ViewListener getViewListener() {
        return viewListener;
    }

    /**
     * Set the view listener
     * @param viewListener The new view listener
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    /**
     * The MainActivityLayout constructor
     * @param context The context
     * @param attrs The attrs
     */
    public MainActivityLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initLayout();
    }


    /**
     * Initialize the views
     */
    public void initLayout() {

        button1Button = (FunButton) findViewById(R.id.activity_main_player1_button);
        button2Button = (FunButton) findViewById(R.id.activity_main_player2_button);
        player1TextView = (TextView) findViewById(R.id.rahat);
        player2TextView =(TextView) findViewById(R.id.rahat_2);
        replayPlayer1Button= (FunButton) findViewById(R.id.activity_main_player1_replay_button);
        replayPlayer2Button= (FunButton) findViewById(R.id.activity_main_player2_replay_button);
        button1Button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getModel().getContorPlayer1() < getModel().getLimit() && getModel().getContorPlayer2() < getModel().getLimit())
                    getViewListener().onPlayer1ButtonClicked();
            }
        });
        button2Button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getModel().getContorPlayer2() <getModel().getLimit() && getModel().getContorPlayer1() < getModel().getLimit())
                getViewListener().onPlayer2ButtonClicked();
            }
        });
        replayPlayer1Button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onReplayPlayerButtonClicked();
            }
        });
        replayPlayer2Button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getViewListener().onReplayPlayerButtonClicked();
            }
        });
    }

    @Override
    public void onChange() {
        updateLayout();
    }

    /**
     * Update the layout
     */
    public void updateLayout() {
        if(getModel().getContorPlayer1() == getModel().getLimit()) {
            textViewColorAndString(player1TextView, getResources().getColor(R.color.green), R.string.winner_finish_message);
            textViewColorAndString(player2TextView, getResources().getColor(R.color.red), R.string.loser_finish_message);
            replayButtonsVisibility(VISIBLE);
            buttonsSetText(R.string.finish_message_button);
        }
        else if(getModel().getContorPlayer2() == getModel().getLimit()) {
            textViewColorAndString(player1TextView, getResources().getColor(R.color.red), R.string.loser_finish_message);
            textViewColorAndString(player2TextView, getResources().getColor(R.color.green), R.string.winner_finish_message);
            replayButtonsVisibility(VISIBLE);
            buttonsSetText(R.string.finish_message_button);
        }
        else if(getModel().getContorPlayer1() == -1 || getModel().getContorPlayer2() == -1) {
            textViewColorAndString(player1TextView, getResources().getColor(R.color.gen_black), "Tap to " + getModel().getLimit() + " to win");
            textViewColorAndString(player2TextView, getResources().getColor(R.color.gen_black), "Tap to " + getModel().getLimit() + " to win");
            buttonsSetText(R.string.start_button_text);
        }
        else{
            textViewColorAndString(player1TextView, getResources().getColor(R.color.gen_black), "" + getModel().getContorPlayer1());
            textViewColorAndString(player2TextView,getResources().getColor(R.color.gen_black),"" + getModel().getContorPlayer2());
            replayButtonsVisibility(GONE);
            buttonsSetText(R.string.tap_button_text);
        }
    }

    /**
     * Sets buttons text
     * @param string The text written on the button
     */
    public void buttonsSetText(int string) {
        button1Button.setText(string);
        button2Button.setText(string);
    }

    /**
     * Sets replay buttons visibility
     * @param visibility The visibility for replay buttons
     */
    public void replayButtonsVisibility(int visibility) {
        replayPlayer1Button.setVisibility(visibility);
        replayPlayer2Button.setVisibility(visibility);
    }

    /**
     * Sets the textView's text color and text
     * @param textView The textView
     * @param color The color
     * @param string The text
     */
    public void textViewColorAndString(TextView textView, int color,String string) {
        textView.setTextColor(color);
        textView.setText(string);
    }

    /**
     * Sets the textView's text color and text
     * @param textView The textView
     * @param color The color
     * @param string The text (int)
     */
    public void textViewColorAndString(TextView textView, int color,int string) {
        textView.setTextColor(color);
        textView.setText(string);
    }

    /**
     * ViewListener interface
     */
    public interface ViewListener {
        void onPlayer1ButtonClicked();
        void onPlayer2ButtonClicked();
        void onReplayPlayerButtonClicked();
    }
}
