package com.indyvision.funapp.pojo.models;

import com.indyvision.funapp.activities.MainActivity;
import com.indyvision.funapp.pojo.Interfaces.SimpleObservable;

/**
 * Created by cristian.baita on 7/17/2015.
 */
public class MainActivityModel extends SimpleObservable<MainActivityModel> {
    /**
     * Player one's contor
     */
    private int contorPlayer1;
    /**
     * Player two's contor
     */
    private int contorPlayer2;
    /**
     * The game's number of taps to win
     */
    private int limit;

    /**
     * The constructor
     */
    public MainActivityModel() {
        contorPlayer1=-1;
        contorPlayer2=-1;
        limit=100;
    }

    /**
     * Get the contor for player 1
     * @return The contor for player 1
     */
    public int getContorPlayer1() {
        return contorPlayer1;
    }

    /**
     * Set the contor for player 1
     * @param contorPlayer1 The new value for contor
     * @param isNotificationRequired The notifier
     */
    public void setContorPlayer1(int contorPlayer1, boolean... isNotificationRequired) {
        this.contorPlayer1 = contorPlayer1;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    /**
     * Get the contor for player 2
     * @return The contor for player 2
     */
    public int getContorPlayer2() {
        return contorPlayer2;
    }

    /**
     * Set the contor for player 2
     * @param contorPlayer2 The new value for contor
     * @param isNotificationRequired The notifier
     */
    public void setContorPlayer2(int contorPlayer2, boolean... isNotificationRequired) {
        this.contorPlayer2 = contorPlayer2;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }

    /**
     * Get the number of taps to win
     * @return The number of taps to win
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Sets the number of taps to win the game
     * @param limit The number of taps to win
     * @param isNotificationRequired The notifier
     */
    public void setLimit(int limit, boolean... isNotificationRequired) {
        this.limit = limit;
        if(isNotificationRequired.length > 0 && isNotificationRequired[0]) {
            notifyObservers();
        }
    }
}
